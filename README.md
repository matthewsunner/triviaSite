# triviaSite

This application will allow the user to choose between a Number realated trivia fact, or a random trivia fact. The application uses the fetch call to an external API in order to pull in the trivia information. 

Instructions: 
- Simply click the button of the trivia type you would like to see. 
- Follow on screen prompts to get to the trivia



If you have any questions, please reach out and let me know. 